#ifndef _TEST_A_
#define _TEST_A_

#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define NUM_TESTS 5
#define HEAP 16384
#define QUERY 1024

//returns bool array of un/pass test
//and
//prints the results
void run_tests();

#endif
