#include "test_alloc.h"

static void print_test_res(bool res, size_t n);
static void print_test_start(size_t n);
static struct block_header* block_get_header(void* contents);
static bool test_1(void* heap);
static bool test_2(void* heap);
static bool test_3(void* heap);
static bool test_4(void* heap);
static bool test_5(void* heap);


void run_tests(){
  bool res[NUM_TESTS] = {0};
  void* heap = heap_init(HEAP);
  debug_heap(stdout, heap);
  for(size_t i = 0; i < NUM_TESTS; i++){
    print_test_start(i+1);
    switch (i) {
      case 0:
      res[i] = test_1(heap);
      print_test_res(res[i], i+1);
        break;
      case 1:
      res[i] = test_2(heap);
      print_test_res(res[i], i+1);
        break;
      case 2:
      res[i] = test_3(heap);
      print_test_res(res[i], i+1);
        break;
      case 3:
      res[i] = test_4(heap);
      print_test_res(res[i], i+1);
        break;
      case 4:
      res[i] = test_5(heap);
      print_test_res(res[i], i+1);
        break;
      default:
        print_test_res(0, 0);
        break;
    }
  }
}

static void print_test_res(bool res, size_t n){
    if (res) printf("\nTEST %zu PASSED\n", n);
    else printf("\nTEST %zu FAILED\n", n);
}

static void print_test_start(size_t n){
  printf("\nTEST %zu STARTED\n", n);
}

static bool test_1(void* heap){
  bool res = false;
  void* query = _malloc(QUERY);
  debug_heap(stdout, heap);
  if(query) res = true;
  _free(query);
  return res;
}

static bool test_2(void* heap){
  bool res = false;
  void* query_1 = _malloc(QUERY);
  void* query_2 = _malloc(QUERY);
  void* query_3 = _malloc(QUERY);
  debug_heap(stdout, heap);
  _free(query_2);
  debug_heap(stdout, heap);
  struct block_header* q1 = block_get_header(query_1);
  struct block_header* q2 = block_get_header(query_2);
  struct block_header* q3 = block_get_header(query_3);

  if(!(q1->is_free)&&(q2->is_free)&&!(q3->is_free)) res = true;
  _free(query_1);
  _free(query_3);
  debug_heap(stdout, heap);
  return res;
}

static bool test_3(void* heap){
  bool res = false;
  void* query_1 = _malloc(QUERY);
  void* query_2 = _malloc(QUERY);
  void* query_3 = _malloc(QUERY);
  debug_heap(stdout, heap);
  _free(query_1);
  _free(query_2);
  debug_heap(stdout, heap);
  struct block_header* q1 = block_get_header(query_1);
  struct block_header* q2 = block_get_header(query_2);
  struct block_header* q3 = block_get_header(query_3);
  if(!(q1->is_free)||!(q2->is_free)||(q3->is_free)) res = false;
  else res = true;
  _free(query_3);
  debug_heap(stdout, heap);
  return res;
}

static bool test_4(void* heap){
  bool res = false;
  void* query_1 = _malloc(HEAP - QUERY);
  debug_heap(stdout, heap);
  void* query_2 = _malloc(QUERY*2);
  debug_heap(stdout, heap);
  struct block_header* q2 = block_get_header(query_2);
  if(q2->is_free) res = false;
  else res = true;
  _free(query_1);
  _free(query_2);
  debug_heap(stdout, heap);
  return res;
}

static bool test_5(void* heap){
  bool res = false;
  void* query_1 = _malloc(HEAP - offsetof( struct block_header, contents ));
  debug_heap(stdout, heap);
  struct block_header* q1 = block_get_header(query_1);
  while(q1->next) q1 = q1->next;
  void* query_2 = (void*)(((uint8_t*) q1) + size_from_capacity(q1->capacity).bytes);
  query_2 = mmap(query_2, QUERY, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS,0, 0);

  if(query_2 == MAP_FAILED) {
    _free(query_1);
    return res;
  }

  *((struct block_header *) query_2) = (struct block_header) {
                .next = NULL,
                .capacity = capacity_from_size((block_size) {.bytes = HEAP}),
                .is_free = true
  };

  debug_heap(stdout, query_2);
  void* query_3 = _malloc(QUERY*10);
  debug_heap(stdout, heap);
  if(query_3) res = true;
  _free(query_3);
  debug_heap(stdout, heap);
  return res;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}
